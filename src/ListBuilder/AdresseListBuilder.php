<?php
/**
 * Created by PhpStorm.
 * User: pink
 * Date: 29/06/2016
 * Time: 11:01
 */


namespace Ouat\EntityBundle\ListBuilder;

use Ouat\UIBundle\ListBuilder\ListBuilder;

class AdresseListBuilder extends ListBuilder {

    protected function configure() {

        $this
            ->addFieldFormat('ville', 'field', [
                'label' => "Ville",
            ])
            ->addFieldFormat('codePostal', 'field',['label' => "C.P.",
            ])
            ;
    }
}
