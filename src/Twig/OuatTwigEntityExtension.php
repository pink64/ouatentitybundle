<?php
/**
 * Created by PhpStorm.
 * User: pink
 * Date: 05/03/2015
 * Time: 15:24
 */
namespace Ouat\EntityBundle\Twig;


use JMS\Serializer\SerializationContext;
use Melody\UIBundle\Helper\TabsHelper;
use Ouat\EntityBundle\Interfaces\HasStyleInterface;
use Ouat\UIBundle\Screen\ScreenConfig;
use Ouat\UIBundle\Tools\OX;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\VarDumper\VarDumper;

class OuatTwigEntityExtension extends \Twig_Extension implements ContainerAwareInterface
{
    public function __construct(ContainerInterface $container) {
        $this->setContainer($container);
    }
    /**
     * @var ContainerInterface $container
     */
    protected $container ;

    public function setContainer(ContainerInterface $container = null) {
        $this->container = $container;
    }

    public function getName()
    {
        return 'ouat_entity_ui_extension';
    }

    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('stylify', array($this, 'stylify')),

        );
    }


    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('stylify', array($this, 'stylify'),array('is_safe' => array('html'))),
        );
    }

    public function stylify($value,$style = null,$tag = 'span') {
        if ($style===NULL && is_object($value) && $value instanceof HasStyleInterface)
            $style = $value->getStyle();

        if (!$style)
            return $value ;

        return '<'.$tag.' style="border-radius:10px;padding:5px;'.$style->toHtmlStyle().'">'.$value.'</'.$tag.'>' ;
    }




}