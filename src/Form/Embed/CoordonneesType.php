<?php

namespace Ouat\EntityBundle\Form\Embed;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CoordonneesType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email')
            ->add('mobile',null,['label'=>"Tél. mobile"])
            ->add('telephone',null,['label'=>"Tél. fixe"])
            ->add('fax',null,['label'=>"Fax"])
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Ouat\EntityBundle\Entity\Embed\Coordonnees'
        ));
    }

    public function getBlockPrefix()
    {
        return 'ouat_entity_coordonnees' ;
    }
}
