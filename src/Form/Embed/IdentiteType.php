<?php

namespace Ouat\EntityBundle\Form\Embed;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class IdentiteType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder

            ->add('genre',ChoiceType::class,array(
                'choices' => ['M'=>'M','Mme'=>'Mme','Mlle'=>'Mlle'],
                'expanded' => true,
                'widget_type' => 'inline',
            ))
            ->add('nom',null,['label'=>"Nom"])
            ->add('prenom',null,['label'=>"Prénom"])
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Ouat\EntityBundle\Entity\Embed\Identite'
        ));
    }

    public function getBlockPrefix()
    {
        return 'ouat_entity_identite' ;
    }
}
