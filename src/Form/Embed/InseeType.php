<?php

namespace Ouat\EntityBundle\Form\Embed;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class InseeType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder

            ->add('denomination',null,['label'=>"Dénomination"])
            ->add('siren',null,['label'=>"SIREN"])
            ->add('tva_intracommunautaire',null,['label'=>"TVA Intra."])
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Ouat\EntityBundle\Entity\Embed\Insee'
        ));
    }

    public function getBlockPrefix()
    {
        return 'ouat_entity_insee' ;
    }
}
