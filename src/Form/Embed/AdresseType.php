<?php

namespace Ouat\EntityBundle\Form\Embed;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AdresseType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('adresse1')
            ->add('adresse2',null,['label'=>"Compl. adresse"])
            ->add('adresse3',null,['label'=>"Compl. adresse"])
            ->add('codePostal')
            ->add('ville')
            ->add('pays')
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Ouat\EntityBundle\Entity\Embed\Adresse'
        ));
    }

    public function getBlockPrefix()
    {
        return 'ouat_entity_adresse' ;
    }
}
