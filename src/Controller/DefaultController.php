<?php

namespace Ouat\EntityBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class DefaultController extends Controller
{
//   
    public function indexAction()
    {
        return $this->render('OuatEntityBundle:Default:index.html.twig');
    }
}
