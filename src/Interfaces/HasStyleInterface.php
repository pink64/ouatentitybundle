<?php
/**
 * Created by PhpStorm.
 * User: pink
 * Date: 09/06/2016
 * Time: 15:20
 */


namespace Ouat\EntityBundle\Interfaces;

use Ouat\EntityBundle\Entity\Embed\Style;

Interface HasStyleInterface {
    /**
     * @return Style
     */
    public function getStyle();
}