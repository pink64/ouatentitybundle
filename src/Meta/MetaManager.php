<?php
/**
 * Created by PhpStorm.
 * User: pink
 * Date: 27/06/2016
 * Time: 10:59
 */


namespace Ouat\EntityBundle\Meta;


use JMS\Serializer\Annotation as Serializer;
use Ouat\EntityBundle\Meta\MetaApplication;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\PropertyAccess\PropertyAccessor;

class MetaManager  {


    /**
     * @var ContainerInterface
     */
    protected $container ;

    /**
     * @var MetaApplication
     * @Serializer\Type(array<EntityDescription>)
     */
    protected $application ;

    /**
     * @var array
     */
    protected $config ;

    public function __construct(ContainerInterface $container)
    {
        $this->setContainer($container);
    }

//    /**
//     * @return mixed
//     */
//    public function getConfig()
//    {
//        return $this->config;
//    }
//
    /**
     * @param mixed $config
     */
    public function setConfig($config)
    {
        print_r($config);
        $this->config = $config;
        $serializer = $this->container->get('jms_serializer');
        $this->application = $serializer->deserialize($config, 'Ouat\EntityBundle\Meta\MetaApplication', 'array');
    }

    /**
     * @return ContainerInterface
     */
    public function getContainer()
    {
        return $this->container;
    }

    /**
     * @param ContainerInterface $container
     */
    public function setContainer($container)
    {
        $this->container = $container;
    }

    public function getApp() {
        return $this->application ;
    }

    /**
     * @return MetaApplication
     */
    public function getApplication()
    {
        return $this->application;
    }

    public function find($path) {
        $pa = PropertyAccess::createPropertyAccessor();
        return $pa->getValue($this,$path);
    }





}