<?php
/**
 * Created by PhpStorm.
 * User: pink
 * Date: 29/06/2016
 * Time: 09:29
 */

namespace Ouat\EntityBundle\Meta;

class MetaItem {

    protected $parent ;

    /**
     * @return mixed
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @param mixed $parent
     */
    public function setParent($parent)
    {
        $this->parent = $parent;
    }

    
}