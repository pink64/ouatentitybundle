<?php
/**
 * Created by PhpStorm.
 * User: pink
 * Date: 29/06/2016
 * Time: 09:29
 */

namespace Ouat\EntityBundle\Meta;

use JMS\Serializer\Annotation as Serializer;

class MetaApplication extends MetaItem {

    /**
     * @Serializer\Type("array<string,Ouat\EntityBundle\Meta\MetaEntity>")
     * @var array|MetaEntity[]
     */
    protected $entities = array();


    /**
     * @Serializer\PostDeserialize()
     */
    protected function postSerialize() {
        
        foreach($this->getEntities() as $item)
            $item->setParent($this);
    }


    /**
     * @return array|MetaEntity[]
     */
    public function getEntities()
    {
        return $this->entities;
    }


    /**
     * @param $code
     * @return mixed|MetaEntity
     */
    public function getEntity($code) {
        return $this->entities[$code] ;
    }

}