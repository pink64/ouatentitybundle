<?php
/**
 * Created by PhpStorm.
 * User: pink
 * Date: 29/06/2016
 * Time: 09:29
 */

namespace Ouat\EntityBundle\Meta;

use JMS\Serializer\Annotation as Serializer;

class MetaEntity extends MetaItem {

    /**
     * @Serializer\Type("string")
     * @var string
     */
    protected $label ;

    /**
     * @Serializer\Type("string")
     * @var string
     */
    protected $code ;

    /**
     * @Serializer\Type("array<string,Ouat\EntityBundle\Meta\MetaEntityAction>")
     * @var array|MetaEntity[]
     */
    protected $actions = array();

    /**
     * @Serializer\PostDeserialize()
     */
    protected function postSerialize() {

        foreach($this->getActions() as $item)
            $item->setParent($this);
    }

    /**
     * @return array|MetaEntityAction[]
     */
    public function getActions()
    {
        return $this->actions;
    }





    /**
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @param string $label
     */
    public function setLabel($label)
    {
        $this->label = $label;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param string $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }



}