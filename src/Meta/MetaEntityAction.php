<?php
/**
 * Created by PhpStorm.
 * User: pink
 * Date: 29/06/2016
 * Time: 09:29
 */

namespace Ouat\EntityBundle\Meta;

use JMS\Serializer\Annotation as Serializer;

class MetaEntityAction extends MetaItem {

    /**
     * @Serializer\Type("string")
     * @var string
     */
    protected $label ;

    /**
     * @Serializer\Type("string")
     * @var string
     */
    protected $label_menu ;

    /**
     * @Serializer\Type("string")
     * @var string
     */
    protected $route ;

    /**
     * @return mixed
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @param mixed $label
     */
    public function setLabel($label)
    {
        $this->label = $label;
    }

    /**
     * @return mixed
     */
    public function getRoute()
    {
        return $this->route;
    }

    /**
     * @param mixed $route
     */
    public function setRoute($route)
    {
        $this->route = $route;
    }

    /**
     * @return string
     */
    public function getLabelMenu()
    {
        return $this->label_menu;
    }

    /**
     * @param string $label_menu
     */
    public function setLabelMenu($label_menu)
    {
        $this->label_menu = $label_menu;
    }

    


}