<?php
/**
 * Created by PhpStorm.
 * User: pink
 * Date: 27/06/2016
 * Time: 10:59
 */


namespace Ouat\EntityBundle\Format;


use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\PropertyAccess\PropertyAccessor;

class BaseDescriptionItem {

    /**
     * @var ContainerInterface
     */
    protected $container ;

    protected $cache = array();


    protected $config ;
    protected $key ;
    protected $parent ;

    public function __construct(&$config,$parent = NULL,$key = NULL)
    {
        $this->config = $config ;
        $this->parent = $parent ;
        $this->key = $key ;
    }

    /**
     * @return ContainerInterface
     */
    public function getContainer()
    {
        return $this->container;
    }

    /**
     * @param ContainerInterface $container
     */
    public function setContainer($container)
    {
        $this->container = $container;
    }

    /**
     * @return null
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * @param null $key
     */
    public function setKey($key)
    {
        $this->key = $key;
    }

    /**
     * @return null
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @param null $parent
     */
    public function setParent($parent)
    {
        $this->parent = $parent;
    }



    /**
     * @return mixed
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * @param mixed $config
     */
    public function setConfig($config)
    {

        $this->config = $config;
    }

    protected function getCache($type,$key = NULL) {
        $key_type = $this->getTypeKey($type);

        if (!isset($this->cache[$key_type]))
            $this->cache[$key_type] = array();

        if ($key === NULL) {
            $this->cache[$key_type] = $this->createCache($type);
            return $this->cache[$key_type];
        }


        if (!isset($this->cache[$key_type][$key])) {
            $this->cache[$key_type][$key] = $this->createCache($type,$key);
        }

        return $this->cache[$key_type][$key] ;
    }

    protected function getValue($path) {

    }

    protected function getTypeKey($type) {
     return $type ;
    }

    public function __get($property) {

        $ret = $this->getValue($property);

        if ($ret !== NULL)
            return $ret ;

        $pa = PropertyAccess::createPropertyAccessor();
        return $pa->getValue($this->config,'['.$property.']');
        if (property_exists($this, $property)) {
            return $this->$property;
        }
    }







}