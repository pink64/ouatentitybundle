<?php
/**
 * Created by PhpStorm.
 * User: pink
 * Date: 27/06/2016
 * Time: 10:58
 */


namespace Ouat\EntityBundle\Format;

class EntityInfo {

    protected $route_prefix ;
    protected $route_param_name ;
    protected $default_page = 'home' ;

    /**
     * @return mixed
     */
    public function getRoutePrefix()
    {
        return $this->route_prefix;
    }

    /**
     * @param mixed $route_prefix
     */
    public function setRoutePrefix($route_prefix)
    {
        $this->route_prefix = $route_prefix;
    }

    /**
     * @return mixed
     */
    public function getRouteParamName()
    {
        return $this->route_param_name;
    }

    /**
     * @param mixed $route_param_name
     */
    public function setRouteParamName($route_param_name)
    {
        $this->route_param_name = $route_param_name;
    }

    /**
     * @return string
     */
    public function getDefaultPage()
    {
        return $this->default_page;
    }

    /**
     * @param string $default_page
     */
    public function setDefaultPage($default_page)
    {
        $this->default_page = $default_page;
    }

    


}