<?php
/**
 * Created by PhpStorm.
 * User: pink
 * Date: 27/06/2016
 * Time: 10:59
 */


namespace Ouat\EntityBundle\Format;


use Symfony\Component\DependencyInjection\ContainerInterface;

abstract class EntityInfoSeeker {


    /**
     * @var ContainerInterface
     */
    protected $container ;

    protected $cache = array();

    public function __construct(ContainerInterface $container)
    {
        $this->setContainer($container);
    }

    /**
     * @return ContainerInterface
     */
    public function getContainer()
    {
        return $this->container;
    }

    /**
     * @param ContainerInterface $container
     */
    public function setContainer($container)
    {
        $this->container = $container;
    }

    /**
     * @param $record
     * @return EntityInfo
     */
    public function seekEntityInfo($record) {
        $class = get_class($record);

        if (isset($this->cache[$class])) {
            return $this->cache[$class] ;
        }

        $info = new EntityInfo();

        if ($this->fillEntityInfo($info,$record,$class)) {
            $this->cache[$class] = $info;
            return $this->cache[$class] ;
        }

        return NULL ;

    }

    abstract protected function fillEntityInfo(EntityInfo $info,$record,$class = NULL);


}