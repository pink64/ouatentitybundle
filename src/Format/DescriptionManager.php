<?php
/**
 * Created by PhpStorm.
 * User: pink
 * Date: 27/06/2016
 * Time: 10:59
 */


namespace Ouat\EntityBundle\Format;


use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\DependencyInjection\ContainerInterface;

class DescriptionManager extends BaseDescriptionItem {


    /**
     * @var ContainerInterface
     */
    protected $container ;

//    protected $cache = array();

    /**
     * @Serializer\Type(array<EntityDescription>)
     */
    protected $entities = array();


//    protected $config ;

    public function __construct(ContainerInterface $container)
    {
        $this->setContainer($container);

//        $this->config = $this->container->getParameter('ouat_description');
//        var_dump($this->config);
    }

//    /**
//     * @return mixed
//     */
//    public function getConfig()
//    {
//        return $this->config;
//    }
//
//    /**
//     * @param mixed $config
//     */
//    public function setConfig($config)
//    {
//
//        $this->config = $config;
//    }
//
//
//
//    protected function getTypeKey($type) {
//        switch($type) {
//            case 'entity' : return 'entities' ;
//        }
//    }

//    protected function getCache($type,$key) {
//        $key_type = $this->getTypeKey($type);
//
//
//
//        if (!isset($this->cache[$key_type]))
//            $this->cache[$key_type] = array();
//
//        if (!isset($this->cache[$key_type][$key])) {
//            $this->cache[$key_type][$key] = $this->createCache($type,$key);
//        }
//
//        return $this->cache[$key_type][$key] ;
//    }

    protected function createCache($type,$key = NULL) {

        switch($type) {
            case 'entities' :
                $res = array();
                foreach($this->config[$this->getTypeKey($type)] as $ke => $ve) {
                    $res[$ke] = new EntityDescription($ve,$this,$ke);
                }

                return $res ;
                break ;

            case 'entity' :
                error_log("create entity $key");
                $z = new EntityDescription($this->config[$this->getTypeKey($type)][$key],$this,$key);
                return $z ;
                break ;
        }
    }

    public function getEntities() {
        return $this->getCache('entities');
    }

    public function getEntityDescription($entity_code) {
        return $this->getCache('entity',$entity_code);

    }
    /**
     * @return ContainerInterface
     */
    public function getContainer()
    {
        return $this->container;
    }

    /**
     * @param ContainerInterface $container
     */
    public function setContainer($container)
    {
        $this->container = $container;
    }





}