<?php

namespace Ouat\EntityBundle\Command;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Routing\Route;
use Symfony\Component\VarDumper\VarDumper;

class GenerateEntityInfoCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('ouat:generate_entity_info')
            ->setDescription('Hello PhpStorm');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $router = $this->getContainer()->get('router');


        foreach($router->getRouteCollection() as $route_name => $route) {
            $this->checkRoute($input,$output,$route_name,$route);
        }
    }

    /**
     * @return \Doctrine\Common\Annotations\AnnotationReader
     */
    protected function getAnnotationReader() {
        return $this->getContainer()->get('annotations.reader');
    }

    protected function checkRoute(InputInterface $input, OutputInterface $output,$route_name,Route $route) {
        @list($class,$method) = explode('::',$route->getDefault('_controller'));

        if (!$class || !$method)
            return false ;


        if ($route_name!='employe_edit')
            return false ;

        $rc = new \ReflectionClass($class);
        $rm = $rc->getMethod($method);

        $vd = new VarDumper();



        /**
         * @var Security $securityAnnotation
         */

        $securityAnnotation = $this->getAnnotationReader()->getMethodAnnotation($rm,'Sensio\Bundle\FrameworkExtraBundle\Configuration\Security');

        /**
         *
         */

        $params = $this->getAnnotationReader()->getMethodAnnotations($rm);
        $vd->dump($params);





        $output->writeln($route_name." \t$class -> $method");




        $vd = new VarDumper();

//        $vd->dump($route);
    }
}
