<?php

namespace Ouat\EntityBundle\Command;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Routing\Route;
use Symfony\Component\VarDumper\VarDumper;

class TestMetaCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('ouat:test_meta')
            ->setDescription('Hello PhpStorm');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $mm = $this->getContainer()->get('ouat.meta_manager');
        $app = $mm->getApplication();

        $ob = $mm->find($p='app.entities[employe]');

        $output->writeln($p."\t".$ob->getLabel());


        $output->writeln("-----------------------------");

        foreach($app->getEntities() as $ed) {

//            $ed = $dm->getEntityDescription('employe');

            $output->writeln("Entité : ".$ed->getLabel());

            foreach($ed->getActions() as $action) {
//                $output->writeln("\tAction : ".$action->getLabel());
                $output->writeln("\t[".$action->getLabel()."]");

                $output->writeln("\tentité:\t".$action->getParent()->getLabel());
                $output->writeln("\troute:\t".$action->getRoute());
                $output->writeln("\tmenu:\t".$action->getLabelMenu());
                $output->writeln("");
            }

        }






    }

}
