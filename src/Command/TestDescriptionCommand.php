<?php

namespace Ouat\EntityBundle\Command;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Routing\Route;
use Symfony\Component\VarDumper\VarDumper;

class TestDescriptionCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('ouat:test_description')
            ->setDescription('Hello PhpStorm');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $dm = $this->getContainer()->get('ouat.entity.description');

        foreach($dm->getEntities() as $ed) {

//            $ed = $dm->getEntityDescription('employe');

            $output->writeln("Entité : ".$ed->label);

            foreach($ed->actions as $action) {
                $output->writeln("\t".$action->label." on ".$action->getParent()->label);
                $output->writeln("\t".$action->route);
            }

        }






    }

}
