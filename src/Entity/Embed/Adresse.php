<?php
/**
 * Created by PhpStorm.
 * User: pink
 * Date: 01/06/2016
 * Time: 11:28
 */

namespace Ouat\EntityBundle\Entity\Embed;

use Doctrine\ORM\Mapping as ORM;
use Ouat\EntityBundle\Traits\Entity\AdresseFields;

/**
 * @ORM\Embeddable
 */
class Adresse
{
    use AdresseFields;

    public function __toString() {
        return "".$this->codePostal." ".$this->ville ;
    }
}