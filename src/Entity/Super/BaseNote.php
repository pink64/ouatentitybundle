<?php
/**
 * Created by PhpStorm.
 * User: pink
 * Date: 01/06/2016
 * Time: 14:48
 */
namespace Ouat\EntityBundle\Entity\Super;

use Doctrine\ORM\Mapping as ORM;
use Ouat\EntityBundle\Traits\Entity\CategorieFields;
use Ouat\EntityBundle\Traits\Entity\TraceableFields;


/**
 * @ORM\MappedSuperclass
 */
class BaseNote {

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id ;

    use TraceableFields;

    /**
     * @var string
     * @ORM\Column(type="text",nullable=true)
     */
    protected $text ;

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @param string $text
     */
    public function setText($text)
    {
        $this->text = $text;
    }


}