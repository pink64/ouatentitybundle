<?php
/**
 * Created by PhpStorm.
 * User: pink
 * Date: 01/06/2016
 * Time: 14:48
 */
namespace Ouat\EntityBundle\Entity\Super;

use Doctrine\ORM\Mapping as ORM;
use Ouat\EntityBundle\Traits\Entity\CategorieFields;
use Ouat\EntityBundle\Traits\Entity\TraceableFields;


/**
 * @ORM\MappedSuperclass
 */
class BaseTache {

    /**
     * @var string
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id ;

    use CategorieFields;
    use TraceableFields;
}