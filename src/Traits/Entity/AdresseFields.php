<?php
/**
 * Created by PhpStorm.
 * User: pink
 * Date: 01/06/2016
 * Time: 11:24
 */

namespace Ouat\EntityBundle\Traits\Entity;

use Doctrine\ORM\Mapping as ORM;

Trait AdresseFields {

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    protected $adresse1;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    protected $adresse2;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    protected $adresse3;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    protected $codePostal;

    /**
     * @ORM\Column(type="string", length=64, nullable=true)
     */
    protected $ville;

    /**
     * @ORM\Column(type="string", length=64, nullable=true)
     */
    protected $pays;

    /**
     * @return mixed
     */
    public function getAdresse1()
    {
        return $this->adresse1;
    }

    /**
     * @param mixed $adresse1
     */
    public function setAdresse1($adresse1)
    {
        $this->adresse1 = $adresse1;
    }

    /**
     * @return mixed
     */
    public function getAdresse2()
    {
        return $this->adresse2;
    }

    /**
     * @param mixed $adresse2
     */
    public function setAdresse2($adresse2)
    {
        $this->adresse2 = $adresse2;
    }

    /**
     * @return mixed
     */
    public function getAdresse3()
    {
        return $this->adresse3;
    }

    /**
     * @param mixed $adresse3
     */
    public function setAdresse3($adresse3)
    {
        $this->adresse3 = $adresse3;
    }

    /**
     * @return mixed
     */
    public function getCodePostal()
    {
        return $this->codePostal;
    }

    /**
     * @param mixed $codePostal
     */
    public function setCodePostal($codePostal)
    {
        $this->codePostal = $codePostal;
    }

    /**
     * @return mixed
     */
    public function getVille()
    {
        return $this->ville;
    }

    /**
     * @param mixed $ville
     */
    public function setVille($ville)
    {
        $this->ville = $ville;
    }

    /**
     * @return mixed
     */
    public function getPays()
    {
        return $this->pays;
    }

    /**
     * @param mixed $pays
     */
    public function setPays($pays)
    {
        $this->pays = $pays;
    }


}