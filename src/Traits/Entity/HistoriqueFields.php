<?php
/**
 * Created by PhpStorm.
 * User: pink
 * Date: 07/06/2016
 * Time: 11:03
 */


namespace Ouat\EntityBundle\Traits\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\PropertyAccess\PropertyAccess;

/**
 */
Trait HistoriqueFields
{

    /**
     * @var array
     * @ORM\Column(type="json_array", nullable=true)
     */
    protected $history = array();



    /**
     * @return array
     */
    public function getHistory()
    {
        return $this->history;
    }

    /**
     * @param array $history
     */
    public function setHistory($history)
    {
        $this->history = $history;
    }







}