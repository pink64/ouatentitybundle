<?php
/**
 * Created by PhpStorm.
 * User: pink
 * Date: 01/06/2016
 * Time: 11:24
 */

namespace Ouat\EntityBundle\Traits\Entity;

use Doctrine\ORM\Mapping as ORM;

Trait InterfaceFields {

    /**
     * @ORM\Column(type="string", length=128, nullable=true)
     */
    protected $codeInterface;

    /**
     * @return mixed
     */
    public function getCodeInterface()
    {
        return $this->codeInterface;
    }

    /**
     * @param mixed $codeInterface
     */
    public function setCodeInterface($codeInterface)
    {
        $this->codeInterface = $codeInterface;
    }

    


}