<?php
/**
 * Created by PhpStorm.
 * User: pink
 * Date: 02/06/2016
 * Time: 09:13
 */


namespace Ouat\EntityBundle\Traits\Entity;

use Doctrine\ORM\Mapping as ORM;
use Ouat\EntityBundle\Entity\Embed\Adresse;
use Ouat\EntityBundle\Entity\Embed\Coordonnees;
use Ouat\EntityBundle\Entity\Embed\Insee;

Trait SocieteFields {
    use InterfaceFields;

    /**
     * @var Adresse
     * @ORM\Embedded(class = "Ouat\EntityBundle\Entity\Embed\Adresse")
     */
    private $adresse;

    /**
     * @var Insee
     * @ORM\Embedded(class = "Ouat\EntityBundle\Entity\Embed\Insee")
     */
    private $insee;

    /**
     * @var Coordonnees
     * @ORM\Embedded(class = "Ouat\EntityBundle\Entity\Embed\Coordonnees")
     */
    private $coordonnees;

    public function __construct() {
        $this->adresse = new Adresse();
        $this->insee = new Insee();
    }

    public function __toString() {
        return ''.$this->getInsee()->getDenomination();
    }

    /**
     * @return mixed
     */
    public function getAdresse()
    {
        return $this->adresse;
    }

    /**
     * @param mixed $adresse
     */
    public function setAdresse($adresse)
    {
        $this->adresse = $adresse;
    }

    /**
     * @return Insee
     */
    public function getInsee()
    {
        return $this->insee;
    }

    /**
     * @param Insee $insee
     */
    public function setInsee($insee)
    {
        $this->insee = $insee;
    }

    /**
     * @return Coordonnees
     */
    public function getCoordonnees()
    {
        return $this->coordonnees;
    }

    /**
     * @param Coordonnees $coordonnees
     */
    public function setCoordonnees($coordonnees)
    {
        $this->coordonnees = $coordonnees;
    }

    


}