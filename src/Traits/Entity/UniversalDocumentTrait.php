<?php
/**
 * Created by PhpStorm.
 * User: pink
 * Date: 01/07/2016
 * Time: 10:48
 */


namespace Ouat\EntityBundle\Traits;

Trait UniversalDocumentTrait {

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $universal_id;

    /**
     * @return mixed
     */
    public function getUniversalId()
    {
        return $this->universal_id;
    }

    /**
     * @param mixed $universal_id
     */
    public function setUniversalId($universal_id)
    {
        $this->universal_id = $universal_id;
    }


}