<?php
/**
 * Created by PhpStorm.
 * User: pink
 * Date: 01/06/2016
 * Time: 11:24
 */

namespace Ouat\EntityBundle\Traits\Entity;

use Doctrine\ORM\Mapping as ORM;

Trait InseeFields {

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $denomination;

    /**
     * @ORM\Column(type="string", length=32, nullable=true)
     */
    protected $siren;

    /**
     * @ORM\Column(type="string", length=32, nullable=true)
     */
    protected $siretSiege;

    /**
     * @ORM\Column(type="string", length=32, nullable=true)
     */
    protected $tvaIntracommunautaire;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    protected $codeNAF;

    public function __toString() {
        return ''.$this->getDenomination();
    }

    /**
     * @return mixed
     */
    public function getDenomination()
    {
        return $this->denomination;
    }

    /**
     * @param mixed $denomination
     */
    public function setDenomination($denomination)
    {
        $this->denomination = $denomination;
    }

    /**
     * @return mixed
     */
    public function getSiren()
    {
        return $this->siren;
    }

    /**
     * @param mixed $siren
     */
    public function setSiren($siren)
    {
        $this->siren = $siren;
    }

    /**
     * @return mixed
     */
    public function getSiretSiege()
    {
        return $this->siretSiege;
    }

    /**
     * @param mixed $siretSiege
     */
    public function setSiretSiege($siretSiege)
    {
        $this->siretSiege = $siretSiege;
    }

    /**
     * @return mixed
     */
    public function getTvaIntracommunautaire()
    {
        return $this->tvaIntracommunautaire;
    }

    /**
     * @param mixed $tvaIntracommunautaire
     */
    public function setTvaIntracommunautaire($tvaIntracommunautaire)
    {
        $this->tvaIntracommunautaire = $tvaIntracommunautaire;
    }

    /**
     * @return mixed
     */
    public function getCodeNAF()
    {
        return $this->codeNAF;
    }

    /**
     * @param mixed $codeNAF
     */
    public function setCodeNAF($codeNAF)
    {
        $this->codeNAF = $codeNAF;
    }




}