<?php
/**
 * Created by PhpStorm.
 * User: pink
 * Date: 01/06/2016
 * Time: 11:24
 */

namespace Ouat\EntityBundle\Traits\Entity;

use Doctrine\ORM\Mapping as ORM;

Trait StyleFields {

    /**
     * @ORM\Column(type="string", length=24, nullable=true)
     */
    protected $bgColor;

    /**
     * @ORM\Column(type="string", length=24, nullable=true)
     */
    protected $textColor;

    /**
     * @ORM\Column(type="string", length=24, nullable=true)
     */
    protected $icon;

    /**
     * @ORM\Column(type="string", length=24, nullable=true)
     */
    protected $css;

    /**
     * @return mixed
     */
    public function getBgColor()
    {
        return $this->bgColor;
    }

    /**
     * @param mixed $bgColor
     */
    public function setBgColor($bgColor)
    {
        $this->bgColor = $bgColor;
    }

    /**
     * @return mixed
     */
    public function getTextColor()
    {
        return $this->textColor;
    }

    /**
     * @param mixed $textColor
     */
    public function setTextColor($textColor)
    {
        $this->textColor = $textColor;
    }

    /**
     * @return mixed
     */
    public function getIcon()
    {
        return $this->icon;
    }

    /**
     * @param mixed $icon
     */
    public function setIcon($icon)
    {
        $this->icon = $icon;
    }

    /**
     * @return mixed
     */
    public function getCss()
    {
        return $this->css;
    }

    /**
     * @param mixed $css
     */
    public function setCss($css)
    {
        $this->css = $css;
    }

    




    public function toHtmlStyle() {
        $res = array();

        if ($this->getBgColor()) $res[] = 'background-color:'.$this->getBgColor();
        if ($this->getTextColor()) $res[] = 'color:'.$this->getTextColor();

        return implode(';',$res);
    }


    
}