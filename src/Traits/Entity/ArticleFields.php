<?php
/**
 * Created by PhpStorm.
 * User: pink
 * Date: 07/06/2016
 * Time: 11:03
 */


namespace Ouat\EntityBundle\Traits\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\PropertyAccess\PropertyAccess;

/**
 * Class ArticleFields
 * @package Ouat\EntityBundle\Traits\Entity
 *
 * EN CAS D'AJOUT DE CHAMPS:
 *  - PENSER A METTRE A JOUR LA METHODE toArrayArticle
 *  - REFLECHIR SI CELA PEUT AVOIR DES CONSEQUENCES SUR L'ENSEMBLE DES CLASSES UTILISANT CE TRAIT
 * 
 */
Trait ArticleFields
{

    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $designation;

    /**
     * @var string
     * @ORM\Column(type="string",  nullable=true)
     */
    protected $description;

    /**
     * @var string
     * @ORM\Column(type="string", length=64, nullable=true)
     */
    protected $ref_societe ;

    /**
     * @var string
     * @ORM\Column(type="string", length=64, nullable=true)
     */
    protected $ref_fournisseur ;

    /**
     * @var string
     * @ORM\Column(type="string", length=64, nullable=true)
     */
    protected $famille ;

    /**
     * @var string
     * @ORM\Column(type="string", length=8, nullable=true)
     */
    protected $unite = 'U' ;

    /**
     * @var float
     * @ORM\Column(type="float", nullable=true)
     */
    protected $prix_unitaire_ht ;

    public function toArrayArticle() {
        return array(
            'designation' => $this->getDesignation(),
            'description' => $this->getDescription(),
            'ref_societe' => $this->getRefSociete(),
            'ref_fournisseur' => $this->getRefFournisseur(),
            'unite' => $this->getUnite(),
            'prix_unitaire_ht' => $this->getPrixUnitaireHt(),
            'famille' => $this->getFamille()
            );
    }

    public function fromArrayArticle($values) {
        $fields = array(
            'designation' ,
            'description',
            'famille',
            'ref_societe' ,
            'ref_fournisseur',
            'unite' ,
            'prix_unitaire_ht'
        );

        $pa = PropertyAccess::createPropertyAccessor();

        foreach($fields  as $field)
            if (isset($values[$field]))
                $pa->setValue($this,$field,$values[$field]);


    }

    public function importArticleFields($mixed) {
//        $pa = PropertyAccess::createPropertyAccessor();

        foreach($mixed->toArrayArticle() as $k => $v)
            $this->{ $k } = $v ;

    }

    /**
     * @return string
     */
    public function getDesignation()
    {
        return $this->designation;
    }

    /**
     * @param string $designation
     */
    public function setDesignation($designation)
    {
        $this->designation = $designation;
    }

    /**
     * @return string
     */
    public function getRefSociete()
    {
        return $this->ref_societe;
    }

    /**
     * @param string $ref_societe
     */
    public function setRefSociete($ref_societe)
    {
        $this->ref_societe = $ref_societe;
    }

    /**
     * @return string
     */
    public function getRefFournisseur()
    {
        return $this->ref_fournisseur;
    }

    /**
     * @param string $ref_fournisseur
     */
    public function setRefFournisseur($ref_fournisseur)
    {
        $this->ref_fournisseur = $ref_fournisseur;
    }

    /**
     * @return string
     */
    public function getUnite()
    {
        return $this->unite;
    }

    /**
     * @param string $unite
     */
    public function setUnite($unite)
    {
        $this->unite = $unite;
    }

    /**
     * @return float
     */
    public function getPrixUnitaireHt()
    {
        return $this->prix_unitaire_ht;
    }

    /**
     * @param float $prix_unitaire_ht
     */
    public function setPrixUnitaireHt($prix_unitaire_ht)
    {
        $this->prix_unitaire_ht = $prix_unitaire_ht;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getFamille()
    {
        return $this->famille;
    }

    /**
     * @param string $famille
     */
    public function setFamille($famille)
    {
        $this->famille = $famille;
    }





}