<?php
/**
 * Created by PhpStorm.
 * User: pink
 * Date: 01/06/2016
 * Time: 11:24
 */

namespace Ouat\EntityBundle\Traits\Entity;

use Doctrine\ORM\Mapping as ORM;

Trait IdentiteFields {

    /**
     * @ORM\Column(type="string", length=30, nullable=true)
     */
    protected $genre;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    protected $nom;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    protected $prenom;

    public function __toString()
    {
        return ''.$this->getPrenom().' '.$this->getNom() ;
    }

    /**
     * @return mixed
     */
    public function getGenre()
    {
        return $this->genre;
    }

    /**
     * @param mixed $genre
     */
    public function setGenre($genre)
    {
        $this->genre = $genre;
    }

    /**
     * @return mixed
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @param mixed $nom
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    }

    /**
     * @return mixed
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * @param mixed $prenom
     */
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;
    }




    
}