<?php
/**
 * Created by PhpStorm.
 * User: pink
 * Date: 01/06/2016
 * Time: 11:24
 */

namespace Ouat\EntityBundle\Traits\Entity;

use Doctrine\ORM\Mapping as ORM;

Trait CategorieFields {



    /**
     * @ORM\Column(type="string", length=128, nullable=true)
     */
    protected $label;

    use InterfaceFields;

    public function __toString() {
        return $this->getLabel() ? $this->getLabel() : $this->getCodeInterface();
    }

    /**
     * @return mixed
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @param mixed $label
     */
    public function setLabel($label)
    {
        $this->label = $label;
    }





}